FROM debian:11-slim
MAINTAINER Jan <jan.gaus@cyqon.de>

RUN apt update --allow-releaseinfo-change && \
    apt -y install ca-certificates \
                   apt-transport-https

RUN apt update --allow-releaseinfo-change && \
    apt -y install curl \
                   unzip \
                   nginx \
                   supervisor \
                   net-tools \
                   nano cron \
                   php7.4-fpm \
                   php7.4-gd \
                   php7.4-cli \
                   php7.4-curl \
                   php7.4-json \
                   php7.4-mysql \
                   php7.4-opcache \
                   php7.4-mbstring \
                   php7.4-ldap \
                   php7.4-zip \
                   php7.4-xml \
                   php7.4-soap \
                   php7.4-intl \
                   php7.4-imagick && \
   sed -i -e "s/worker_processes  1/worker_processes 8/" /etc/nginx/nginx.conf && \
   echo "daemon off;" >> /etc/nginx/nginx.conf && \
   sed -i 's/memory_limit = [^ ]*/memory_limit = 512M/' /etc/php/7.4/fpm/php.ini && \
   sed -i 's/upload_max_filesize = [^ ]*/upload_max_filesize = 200M/' /etc/php/7.4/fpm/php.ini && \
   sed -i 's/max_execution_time = [^ ]*/max_execution_time = 600/' /etc/php/7.4/fpm/php.ini && \
   sed -i 's/memory_limit = [^ ]*/memory_limit = -1/' /etc/php/7.4/cli/php.ini && \
   sed -i 's/post_max_size = [^ ]*/post_max_size = 200M/' /etc/php/7.4/fpm/php.ini && \
   sed -i 's/pm = [^ ]*/pm = static/' /etc/php/7.4/fpm/pool.d/www.conf && \
   sed -i 's/pm.max_children = [^ ]*/pm.max_children = 8/' /etc/php/7.4/fpm/pool.d/www.conf && \
   sed -i 's/;catch_workers_output = [^ ]*/catch_workers_output = yes/' /etc/php/7.4/fpm/pool.d/www.conf && \
   sed -i 's/;clear_env = [^ ]*/clear_env = no/' /etc/php/7.4/fpm/pool.d/www.conf && \
   sed -i 's/;daemonize = [^ ]*/daemonize = no/' /etc/php/7.4/fpm/php-fpm.conf  && \
   mkdir -p /run/php/ && \
   touch /run/php/php7.4-fpm.sock

ADD ./php-fpm/00-ioncube.ini /etc/php/7.4/fpm/conf.d/00-ioncube.ini
ADD ./php-fpm/00-ioncube.ini /etc/php/7.4/cli/conf.d/00-ioncube.ini
ADD ./php-fpm/ioncube_loader_lin_7.4.so /usr/lib/php/20190902/
ADD ./php-fpm/ioncube_loader_lin_7.4_ts.so /usr/lib/php/20190902/


ADD ./nginx/nginx.conf /etc/nginx/sites-available/default
ADD ./nginx/php_params /etc/nginx/php_params
ADD ./supervisor/supervisord.conf /etc/supervisord.conf

WORKDIR /srv

# CRONTAB
ADD crontab /etc/cron.d/laravel
RUN chmod 0644 /etc/cron.d/laravel

# COMPOSER
RUN curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer && \
    chmod a+x /usr/local/bin/composer

# GULP
RUN apt-get install -y gnupg && \
    curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get install -y nodejs && \
    npm install --global gulp-cli

RUN npm install --global yarn

ENV TZ=Europe/Berlin

EXPOSE 80

CMD env > /etc/environment; supervisord -c /etc/supervisord.conf
